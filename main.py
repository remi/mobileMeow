# hi guys welcome to my tutorial
# today we will send a MISSKEY NOTE using termux

import requests, json, re, termux

db = {}
with open("db.json", "r") as database:
    db = json.load(database)

def findUsers(text, instance):
    users = re.findall("@\S+", text)
    ids = []
    for user in users:
        user = user.split("@")[1:]
        username = user[0]
        host = ""
        if len(user) == 1:
            host = instance
        else:
            host = user[1]
        try:
            r = requests.post(
                f"https://{instance}/api/users/show",
                json={'username': username, 'host': host}
            )
            j = r.json()
            ids.append(j["id"])
        except:
            pass
    return ids

def sendNote(text, apiKey, instance, mode = "public", cw = None):
    r = ""
    jay = {}
    jay['i'] = apiKey
    jay['text'] = text
    jay['visibility'] = mode
    if cw is not None:
        jay['cw'] = cw
    if mode == "specified":
        u = findUsers(text, instance)
        jay['visibleUserIds'] = u
    r = requests.post(
        f"https://{instance}/api/notes/create",
        json=jay
    )
    print(r.text)

# so the loop is

# check latest texts
# compare with old texts
# for new texts
    # split by spaces
    # really we just want the first one
    # check. there. for command
    # valid commands are:
    # post PW p/h/f/d\npost contents
    # postcw PW p/h/f/d\ncontent warning\npost contents
    # pw PW\n[new pw]
    # api PW\n[api key]
    # instance PW [fedi instance domain]

def parseText(number, sms):
    if (number not in db):
        print(f"error: {number} not in database")
        return
    sms = sms.splitlines()
    sms[0] = sms[0].lower().split(" ")
    if (db[number]["pw"] == sms[0][0]):
        try:
            sms[0][2] == sms[0][1]
        except:
            pass
        sms[0][1] == sms[0][0]
        sms[0][0] == "post"
    if (db[number]["pw"] != sms[0][1]):
        print(f"auth error on {number}, {db[number]['pw']} != {sms[0][1]}")
    if (sms[0][0] == "post" or sms[0][0] == "postcw"):
        mode = "public"
        try:
            if (sms[0][2] == "p"):
                mode = "public"
            if (sms[0][2] == "h"):
                mode = "home"
            if (sms[0][2] == "f"):
                mode = "followers"
            if (sms[0][2] == "d"):
                mode = "specified"
        except:
            pass
        sendNote(
            "\n".join(sms[1:]) if sms[0][0] == "post" else "\n".join(sms[2:]),
            db[number]['api'],
            db[number]['instance'],
            mode,
            sms[1] if sms[0][0] == "postcw" else None
        )

import time

messages = termux.sms_list(limit=3, type="inbox")

def main(messages):
    mOld = messages
    mNew = termux.sms_list(limit=3, type="inbox")
    mCha = []
    for new in mNew:
        good = True
        for old in mOld:
            if new == old:
                good = False
        if good:
            mCha.append(new)
    for changed in mCha:
        print(changed)
        parseText(changed['number'], changed['body'])
    return mNew

while True:
    messages = main(messages)
    time.sleep(10)
